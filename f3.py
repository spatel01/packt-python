# find square root of any number

number = int(input('Enter number : '))

if number>0:
    print(f'Square root of {number} is',number**(1/2))
else:
    print('Invalid input',end=" : ")
    print('only positive number accepted!')